package cs102;

public class Account {
    int number;
    double balance;
    String currency;
    double interestRate;

    // Constructors
    public Account() {

    }

    public Account(int n, double b, String c, double i) {
        number = n;
        balance = b;
        currency = c;
        interestRate = i;
    }

    public Account(int n, String c) {
        number = n;
        balance = 0;
        currency = c;
        interestRate = 0;
    }

    public Account(int n) {
        number = n;
        balance = 0;
        currency = "TL";
        interestRate = 0;
    }

    public Account(int n, double b, String c) {
        number = n;
        balance = b;
        currency = c;
        interestRate = 0;
    }

    public Account(int n, String c, double i) {
        number = n;
        balance = 0;
        currency = c;
        interestRate = i;
    }

    public void deposit(double d) {
        balance = balance + d;
    }

    public void deposit() {
        balance = balance + 0;
    }

    public void report() {
        System.out.println("Account " + number +
                " has " + balance + " " +
                currency + ".");
    }
}
