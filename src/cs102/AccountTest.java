package cs102;

public class AccountTest {

    public static void main(String[] args) {
        Account account1 = new Account(1, 100, "TL");
        Account account2 = new Account(2);

        account2.deposit(100);
        account1.deposit();

        account1.report();
        account2.report();
    }
}
